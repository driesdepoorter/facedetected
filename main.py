import numpy as np
import cv2

from picamera.array import PiRGBArray
from picamera import PiCamera


faceCascade = cv2.CascadeClassifier('/home/pi/Desktop/facedetected/haarcascade_frontalface_alt.xml')


# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (1024, 600)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(1024, 600))

secondsFound = 0
ready = False

cv2.namedWindow("window", cv2.WINDOW_NORMAL)
cv2.resizeWindow('window', 1024,600)

cv2.setWindowProperty('window', cv2.WND_PROP_FULLSCREEN, 1)


cv2.moveWindow('window',0,0)

#FONT OPTIOS
font                   = cv2.FONT_HERSHEY_SIMPLEX
fontScale              = 1
fontColor              = (0,0,0)
lineType               = 2



for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):

    img = frame.array
    img = cv2.flip(img, 1)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    if secondsFound >= 60 and ready == False:
        cv2.imwrite('match.png',img)
        print "IMAGE SAVED"
        ready = True

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.2,
        minNeighbors=5, # IF LOWER DETECT MORE
        minSize=(200, 200) # IF LOWER DETECT EVERYTHING
        )


    for (x,y,w,h) in faces:
        cv2.rectangle(gray,(x,y),(x+w,y+h),(0,0,0),8)
        cv2.line(gray,(x,y),(x+w,y),(0,0,0),4)

        if ready == False:
            cv2.putText(gray,"seconds: " + str(secondsFound),
                (x, y - 10),
                font,
                fontScale,
                fontColor,
                lineType)


    print faces

    if len(faces) == 0:
        print "no face"
        secondsFound = 0
    else:
        secondsFound = secondsFound + 1

    print secondsFound

    #TOP BAR
    cv2.rectangle(gray,(0,0),(1024,50),(0,255,0),cv2.cv.CV_FILLED)

    if ready == False:
        if secondsFound >= 60:
            print("MATCH")
            cv2.putText(gray, 'STOP!',
                                    (1024/2 - 20, 35),
                                    font,
                                    fontScale,
                                    (255,255,255),
                                    lineType)

        if secondsFound < 60:
            cv2.putText(gray, 'continue sculpting',
                            (1024/2 - 130, 35),
                            font,
                            fontScale,
                            (255,255,255),
                            lineType)

    if ready == True:
        cv2.putText(gray, 'STOP!',
                                (1024/2 - 20, 35),
                                font,
                                fontScale,
                                (255,255,255),
                                lineType)


    #cv2.line(gray,(0,0),(1300,0),(0,0,0),100)



    #cv2.rectangle(gray,(0,0),(200,200),(255,255,255),5)






    cv2.imshow('window',gray)


    #cv2.imshow("Frame", gray)
    key = cv2.waitKey(1) & 0xFF

	# clear the stream in preparation for the next frame
    rawCapture.truncate(0)

	# if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break

cap.release()
cv2.destroyAllWindows()
